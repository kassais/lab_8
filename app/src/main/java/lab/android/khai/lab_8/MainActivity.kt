package lab.android.khai.lab_8

import android.content.Context
import android.hardware.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.TextView
import android.widget.Toast
import java.text.MessageFormat

class MainActivity : AppCompatActivity() {

    lateinit private var textView: TextView

    lateinit private var toast: Toast

    lateinit private var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sensorPull = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        val acc = sensorPull.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)


        val textView = mutableListOf<TextView>(findViewById<TextView>(R.id.xTarget), findViewById<TextView>(R.id.yTarget),
                findViewById<TextView>(R.id.zTarget), findViewById<TextView>(R.id.orient) )

        sensorPull.registerListener(ListenToAccelerometer(textView), acc, SensorManager.SENSOR_DELAY_NORMAL)

    }


    class ListenToAccelerometer(var views: MutableList<TextView>) : SensorEventListener {
        override fun onSensorChanged(p0: SensorEvent?) {

            val x = p0!!.values[0]
            val y = p0!!.values[1]
            val z = p0!!.values[2]

            views[0].text = "X:" + x
            views[1].text = "Y:" + y
            views[2].text = "Z:" + z

            when{
               y < 4.1 -> views[3].text = "Альбомная ориентация"
                else -> views[3].text = "Портретная ориентация"
            }

            if(z > 8.95 ) {

                views[3].append("\nТелефон в горизонтальном положении")
            }

        }

        override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
            /**
             * not implemented
             * */
        }
    }

}